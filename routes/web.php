<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ExpertController@index')->name('welcome');

Auth::routes();

Route::get('/home', 'ExpertController@index')->name('home');


//Experts Route
Route::post('/get-expert-appointment-modal','ExpertController@showAppointment');
Route::post('/get-expert-available-hours','ExpertController@availableHours');

//Appointment Route
Route::post('/make-appointment','AppointmentController@store');