<?php

namespace Tests\Unit\Traits;

use PHPUnit\Framework\TestCase;
use App\Traits\HasLocalDateTime;
use Carbon\Carbon;

class HasLocalDateTimeTraitTest extends TestCase
{

    protected $some_class ;
    public function setUp():void{
        parent::setUp();
        $this->some_class = new class { use HasLocalDateTime; }; // anonymous class
    }

    
   public function test_convert_time_to_timezone(){
       $from_timezone = "Asia/Damascus";
       $to_timezone = "Asia/Tokyo";
       $time_in_syria = "21:00:00";//this should be 3:00:00  in Japan;
       $expected_time = Carbon::createFromFormat('H:i:s','3:00:00',$to_timezone)->format('H:i:s');
       $result = $this->some_class->convertTimeToTimezone($time_in_syria,$from_timezone,$to_timezone)->format('H:i:s');
       $this->assertEquals($result,$expected_time);
   }
   public function test_convert_dateTime_to_timezone(){
        $from_timezone = "Asia/Damascus";
        $to_timezone = "Asia/Tokyo";
        $time_in_syria = "2020-05-03 21:00:00";//this should be 2020-05-04 3:00 AM in Japan;
        $expected_time = Carbon::createFromFormat('Y-m-d H:i:s','2020-05-04 3:00:00',$to_timezone);
        $result = $this->some_class->convertDateTimeToTimezone($time_in_syria,$from_timezone,$to_timezone);
        $this->assertEquals($result,$expected_time);
    }


    public function test_convert_time_from_utc(){
        $to_timezone = "Asia/Damascus";
        $time_in_utc = "1:00:00"; //1:00 AM should be 4:00 AM in syria
        $expected_time = Carbon::createFromFormat('H:i:s','4:00:00',$to_timezone)->format('H:i:s');
        $result = $this->some_class->convertTimeFromUTC($time_in_utc,$to_timezone)->format('H:i:s');
        $this->assertEquals($result,$expected_time);
    }

    public function test_convert_time_to_utc(){
        $from_timezone = "Asia/Damascus";
        $time_in_syria = "4:00:00"; //4:00 AM should be 1:00 AM in utc
        $expected_time = Carbon::createFromFormat('H:i:s','1:00:00','utc')->format('H:i:s');
        $result = $this->some_class->convertTimeToUTC($time_in_syria,$from_timezone)->format('H:i:s');
        $this->assertEquals($result,$expected_time);
    }

    public function test_convert_datetime_from_utc(){
        $to_timezone = "Asia/Damascus";
        $time_in_utc = '2020-05-11 17:00:00'; //2020-05-11 5:00 PM should be 2020-05-11 8:00 PM in syria
        $expected_time = Carbon::createFromFormat('Y-m-d H:i:s','2020-05-11 20:00:00',$to_timezone)->format('Y-m-d H:i:s');
        $result = $this->some_class->convertDateTimeFromUTC($time_in_utc,$to_timezone)->format('Y-m-d H:i:s');
        $this->assertEquals($result,$expected_time);
    }

    public function test_convert_datetime_to_utc(){
        $from_timezone = "Asia/Damascus";
        $time_in_syria = '2020-05-11 20:00:00'; //2020-05-11 8:00 PM should be 2020-05-11 17:00 PM in utc
        $expected_time = Carbon::createFromFormat('Y-m-d H:i:s','2020-05-11 17:00:00',$from_timezone)->format('Y-m-d H:i:s');
        $result = $this->some_class->convertDateTimeToUTC($time_in_syria,$from_timezone)->format('Y-m-d H:i:s');
        $this->assertEquals($result,$expected_time);
    }

}
