<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use App\Services\ExpertService;
use App\Models\Expert;
use App\Models\Appointment;
class ExpertServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $expert_service;
    public function setUp():void{
        parent::setUp();
        $this->expert_service =  new ExpertService();
    }
    


    public function test_it_can_get_all_experts(){
        $expert = factory(Expert::class,1)->create();

        $result = $this->expert_service->all();

        $this->assertNotEmpty($result);

    }

    public function test_it_can_find_an_expert(){
        $expert = factory(Expert::class,1)->create()[0];
       
        $result = $this->expert_service->find($expert->id);
       
        $this->assertEquals($result->id,$expert->id);

    }

    public function test_it_will_return_available_hours_for_an_expert_with_no_appointment_yet(){
        $expert = factory(Expert::class,1)->create([
            'time_zone'=>'Asia/Tokyo',
            'working_hours_from'=>"9:00:00",
            'working_hours_to'=>"12:00:00",
        ])[0];

        $duration= 60;//1 hour
        $date="2020-05-10";//it's of format Y-m-d
        session()->put('timezone','Asia/Damascus');
        
        $expected_array = [
            [
                "text" => "03:00 AM - 04:00 AM",
                "value" => "03:00:00"
            ],
            [
                "text" => "04:00 AM - 05:00 AM",
                "value" => "04:00:00"
            ],
            [
                "text" => "05:00 AM - 06:00 AM",
                "value" => "05:00:00"
            ],

        ];
        $result = $this->expert_service->get_available_hours($expert,$date,$duration);
        $this->assertEquals($result,$expected_array);

    }

    public function test_it_will_return_available_hours_for_an_expert_with_appointment_existance(){
        $expert = factory(Expert::class,1)->create([
            'time_zone'=>'Asia/Tokyo',
            'working_hours_from'=>"9:00:00",
            'working_hours_to'=>"12:00:00",
        ])[0];

        $client_timezone= 'Asia/Damascus';
        $duration= 60;//1 hour
        $date="2020-05-11";//it's of format Y-m-d
        session()->put('timezone',$client_timezone);
     
        $reserved_time_in_utc=Carbon::createFromFormat('H:i:s',"5:00:00",$client_timezone)->setTimezone('UTC')->format('H:i:s');
        $reserved_datetime_in_utc=Carbon::createFromFormat('Y-m-d H:i:s',$date.' '.'5:00:00',$client_timezone)->setTimezone('UTC');
        $reserved_start_datetime_in_utc=$reserved_datetime_in_utc->format('Y-m-d H:i:s');
        $reserved_end_datetime_in_utc=$reserved_datetime_in_utc->copy()->addMinutes($duration)->format('Y-m-d H:i:s');
        $appointment = factory(Appointment::class,1)->create([
            'expert_id'=>$expert->id,
            'client_timezone'=>$client_timezone,
            'time'=>$reserved_time_in_utc,
            'date'=>$date,
            'duration'=>$duration,
            'start_datetime'=>$reserved_start_datetime_in_utc,
            'end_datetime'=>$reserved_end_datetime_in_utc,

        ])[0];
       
        $expected_array = [
            [
                "text" => "03:00 AM - 04:00 AM",
                "value" => "03:00:00"
            ],
            [
                "text" => "04:00 AM - 05:00 AM",
                "value" => "04:00:00"
            ],
        ];

        $result = $this->expert_service->get_available_hours($expert,$date,$duration);
        $this->assertEquals($result,$expected_array);

    }


    public function tearDown():void
    {
    }
}
