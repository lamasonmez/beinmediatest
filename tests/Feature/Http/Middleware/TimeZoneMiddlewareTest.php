<?php

namespace Tests\Feature\Http\Middleware;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TimeZoneMiddlewareTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_session_has_correct_time_zone_for_remote_ip()
    {
        //this ip from canada with America/Vancouver timezone
        $response = $this->get('/', ['REMOTE_ADDR' => '216.19.176.114']);

        $response->assertSessionHas('timezone','America/Vancouver');

        $response->assertStatus(200);
    }
    public function test_session_has_syria_time_zone_for_local_ip()
    {
        //this ip for localhost
        $response = $this->get('/', ['REMOTE_ADDR' => '127.0.0.1']);

        $response->assertSessionHas('timezone','Asia/Damascus');

        $response->assertStatus(200);
    }
}
