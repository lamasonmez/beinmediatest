<?php

namespace App\View\Components\expert;

use Illuminate\View\Component;

class ExpertComponent extends Component
{

    /**
     * Expert Object.
     *
     * @var \App\Models\Expert
     */
    public $expert;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($expert)
    {
        //
        $this->expert = $expert;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.expert.expert-component');
    }
}
