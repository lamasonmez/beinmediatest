<?php
namespace App\Services;

use App\Models\Appointment;
use App\Traits\HasLocalDateTime;
use Carbon\Carbon;

class AppointmentService {

    use HasLocalDateTime;

   public function create($data){
     $start_datetime = $this->convertDateTimeToUTC($data['date'].' '.$data['time'],session()->get('timezone'));
     $data['time']= $this->convertTimeToUTC($data['time'],session()->get('timezone'))->format('H:i:s');
     $data['start_datetime']= $start_datetime->format('Y-m-d H:i:s');
     $data['end_datetime']=$start_datetime->copy()->addMinutes($data['duration'])->format('Y-m-d H:i:s');
       
     return Appointment::create($data);
   }
}