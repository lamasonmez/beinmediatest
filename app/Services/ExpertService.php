<?php
namespace App\Services;

use App\Models\Expert;

use App\Traits\HasLocalDateTime;

use Carbon\Carbon;

class ExpertService {

    use HasLocalDateTime;

    public function all(){
        return  Expert::cursor();
    }

    public function find($id){
        return  Expert::where('id',$id)->first();
    }

    public function get_available_hours(Expert $expert,$date,$duration){
        // search for appointments
        $appointments = $expert->appointments()->whereDate('date','=',$date)->get();
        $start=$this->convertDateTimeToTimezone($date.' '.$expert->working_hours_from
                                                ,$expert->time_zone
                                                ,session()->get('timezone'));


        $end=$this->convertDateTimeToTimezone($date.' '.$expert->working_hours_to
                                            ,$expert->time_zone
                                            ,session()->get('timezone'));

        $hours=$this->build_hours_array($date,$start,$end,$duration,$appointments);
      
        return $hours;
    }


    protected function build_hours_array($date,$start,$end,$duration,$appointments){
        $hours=[];
        while($start->lessThanOrEqualTo($end)){
            $new_start = $start->copy();
            $new_start->addMinutes($duration);

            //convert datetime to utc and search for it in appointments
          
            $date_tocheck = Carbon::createFromFormat('Y-m-d H:i:s',$date.' '.$start->format('H:i:s'),session()->get('timezone'))->setTimeZone('UTC')->format('Y-m-d H:i:s');
            $booked = $appointments->where('start_datetime','<=',$date_tocheck)
                         ->where('end_datetime','>',$date_tocheck)->first();
            if($booked==null){
               
                if($new_start->lessThanOrEqualTo($end)){
                    array_push($hours,[
                        "text"=>$start->format('h:i A').' - '.$new_start->format('h:i A'),
                        "value"=>$start->format('H:i:s') ,
                     ]);
                }
            }
           
            $start = $new_start;
        }
        return $hours;
    }
    
}