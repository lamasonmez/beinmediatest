<?php


namespace App\Traits;


use Carbon\Carbon;

trait HasLocalDateTime {

    /**
     * Convert a Time fom timezone to antoher'
     * 
     * @param string $timeField
     * @param string $from_tz
     * @param string $to_toz
     * @return Carbon
     */
    public function convertTimeToTimezone($timeField,$from_tz,$to_tz)
    {   
        $date = Carbon::createFromFormat('H:i:s',$timeField,$from_tz)->setTimezone($to_tz);
        return $date;
    }


     /**
     * Localize a DateTime from timezone to antoher
     * 
     * @param string $dateTimeField
     * @param string $from_tz
     * @param string $to_toz
     * @return Carbon
     */
    public function convertDateTimeToTimezone($dateTimeField,$from_tz,$to_tz)
    {   
        $date = Carbon::createFromFormat('Y-m-d H:i:s',$dateTimeField,$from_tz)->setTimezone($to_tz);
        return $date;
    }

    /**
     * Convert a Time From UTC to timezone 
     * 
     * @param string $timeField
     * @param string $timezone
     * @return Carbon
     */
    public function convertTimeFromUTC($timeField,$timezone)
    {   
        $time=Carbon::createFromFormat('H:i:s',$timeField,'UTC')->setTimezone($timezone);
        return $time;
    }

    /**
     * Convert a Time From a timezone to UTC to 
     * 
     * @param string $timeField
     * @param string $timezone
     * @return Carbon
     */
    public function convertTimeToUTC($timeField,$timezone)
    {   
        $time=Carbon::createFromFormat('H:i:s',$timeField,$timezone)->setTimezone('UTC');
        return $time;
    }

    /**
     * Convert a DateTime From UTC to timezone 
     * 
     * @param string $DatetimeField
     * @param string $timezone
     * @return Carbon
     */
    public function convertDateTimeFromUTC($DatetimeField,$timezone)
    {   
        $time=Carbon::createFromFormat('Y-m-d H:i:s',$DatetimeField,'UTC')->setTimezone($timezone);
        return $time;
    }

    /**
     * Convert a DateTime From a timezone to UTC to 
     * 
     * @param string $DatetimeField
     * @param string $timezone
     * @return Carbon
     */
    public function convertDateTimeToUTC($DatetimeField,$timezone)
    {   
        $time=Carbon::createFromFormat('Y-m-d H:i:s',$DatetimeField,$timezone)->setTimezone('UTC');
        return $time;
    }


   
   

}

?>