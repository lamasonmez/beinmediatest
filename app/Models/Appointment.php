<?php

namespace App\Models;

use App\Models\Expert;

class Appointment extends BaseModel
{
    //

    protected $fillable = [
        'client_name',
        'date',
        'time',
        'duration',
        'client_timezone',
        'expert_id',
        'start_datetime',
        'end_datetime'
    ];



    public function expert(){
        return $this->belongsTo(Expert::class);
    }
}
