<?php

namespace App\Models;

use App\Models\Appointment;
use  App\Traits\HasLocalDateTime;

class Expert extends BaseModel
{
    //
    use HasLocalDateTime;

    protected $fillable=["name","expert","country","time_zone","working_hours_from","working_hours_to"];


    public function appointments(){
        return $this->hasMany(Appointment::class);
    }


}
