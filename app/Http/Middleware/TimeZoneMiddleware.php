<?php

namespace App\Http\Middleware;
use \Illuminate\Http\Request;
use Closure;

class TimeZoneMiddleware
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        $ip = $this->getClientIp();
        $timezone = $this->getTimeZone($ip);
        $this->setTimeZoneSession($timezone);
        return $next($request);
    }
    
    /**
    * Get Visitor Ip 
    *
    * @return string
    */
    protected function getClientIp(){
        $ip = \request()->ip();
        return $ip == '127.0.0.1' ? '82.100.144.240' : $ip;
    }
    
    /**
    * Gets Visitor Timezone.
    *
    * @param string $ip
    * @return string
    */
    protected function getTimeZone($ip){
        try {
            $ip_info = json_decode(file_get_contents('http://ip-api.com/json/' . $ip));
            return $ip_info->timezone;
        } 
        catch (\Exception $e) {}
    }
    
    /**
    * Sets Session Timezone .
    *
    * @param string $timezone
    */
    protected function setTimeZoneSession($timezone){
        session()->put('timezone', $timezone);
    }
}
