<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ExpertService;
class ExpertController extends Controller
{
    //
    protected $expert_service;

    public function __construct(ExpertService $exper_service){
        $this->expert_service = $exper_service;
    }

    public function index(){

        $experts = $this->expert_service->all();
        return view('home',compact('experts'));
    }

    public function showAppointment(Request $request){

        $expert = $this->expert_service->find($request->expert_id);
        return view('components.appointment.appointment_modal',compact('expert'));
    }

    public function availableHours(Request $request){

        $expert = $this->expert_service->find($request->expert_id);
        return $this->expert_service->get_available_hours($expert,$request->date,$request->duration);
    }

}
