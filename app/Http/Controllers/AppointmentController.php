<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AppointmentService;
use Carbon\Carbon;
use App\Traits\HasLocalDateTime;
class AppointmentController extends Controller
{
    //
    use HasLocalDateTime;
    protected $appointment_service;

    public function __construct(AppointmentService $appointment_service){
        $this->appointment_service = $appointment_service;
    }

    public function store(Request $request){
        
        $appointment = $this->appointment_service->create($request->all());
        
        $from_time =  $this->convertTimeFromUTC($appointment->time,session()->get('timezone'));
        $to_time = $from_time->copy()->addMinutes($appointment->duration);

        return response()->json([
            'date'=>date('d F Y',strtotime($appointment->date)),
            'from_time'=>$from_time->format('h:i A'),
            'to_time'=>$to_time->format('h:i A')
        ]);
    }
}
