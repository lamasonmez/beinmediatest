<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Appointment;
use App\Models\Expert;
use Faker\Generator as Faker;

$factory->define(Appointment::class, function (Faker $faker) {
    return [
        //
        'id'=>$faker->unique()->randomNumber(8),
        'client_name' => $faker->name,
        'expert_id'=>factory(Expert::class,1)->create()[0]->id,
        'date'=>$faker->date($format='Y-m-d'),
        'time'=> $faker->time($format = 'H:i:s', $timezone = 'UTC'),
        'duration'=>$faker->randomElement(['15','30','45','60']),
        'client_timezone'=>$faker->timezone,
        'start_datetime'=>$faker->dateTime($format='Y-m-d H:i:s',$timezone="UTC"),
        'end_datetime'=>$faker->dateTime($format='Y-m-d H:i:s',$timezone="UTC"),



    ];
});
