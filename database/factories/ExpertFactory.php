<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Expert;
use Faker\Generator as Faker;

$factory->define(Expert::class, function (Faker $faker) {
    return [
        //
        'id'=>$faker->unique()->randomNumber(8),
        'name' => $faker->name,
        'expert'=>$faker->jobTitle,
        'country'=>$faker->country,
        'time_zone'=>$faker->timezone,
        'working_hours_from'=>$faker->time($format = 'H:i:s', $max = 'now'),
        'working_hours_to'=>$faker->time($format = 'H:i:s', $min = 'now'),

    ];
});
