<?php

use Illuminate\Database\Seeder;
use App\Models\Expert;
class ExpertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Expert::firstOrCreate([
            'name' => 'Li Wei',
            'expert'=>'Chinese teacher',
            'country'=>"China",
            'time_zone'=>'Asia/Shanghai',//China Standard Time ( GMT+8 )
            'working_hours_from'=>'9:00:00',//9 AM 
            'working_hours_to'=>'17:00:00'//5 PM
        ]);

        Expert::firstOrCreate([
            'name' => 'Quasi Shawa',
            'expert'=>'Civil engineer',
            'country'=>"Syria",
            'time_zone'=>'Asia/Damascus',//Eastern European Summer Time(GMT+3)
            'working_hours_from'=>'6:00:00',//6 AM
            'working_hours_to'=>'12:00:00'//12 PM
        ]);
        Expert::firstOrCreate([
            'name' => 'Shimaa Badawy',
            'expert'=>'Computer Engineer',
            'country'=>"Egypt",
            'time_zone'=>'Africa/Cairo',//Eastern European Standard Time (GMT+2)
            'working_hours_from'=>'13:00:00',//1 PM 
            'working_hours_to'=>'14:00:00'//2 PM
        ]);
        Expert::firstOrCreate([
            'name' => 'Amida Cho',
            'expert'=>'Computer Engineer',
            'country'=>"Japan",
            'time_zone'=>'Asia/Tokyo',//Eastern European Standard Time (GMT+2)
            'working_hours_from'=>'9:00:00',//9 AM 
            'working_hours_to'=>'21:00:00'//5 PM
        ]);
    }
}
