import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap-datepicker/dist/js/bootstrap-datepicker';

$(function() {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //More Info Button Click on Expert Div
    $('.more-btn').on('click', function() {
        let $this = $(this);
        let data_target = $this.attr('data-target');
        let more_info_id = 'more-info' + data_target;
        let book_now_id = 'book-now' + data_target;
        $this.slideUp(400, function() {
            $('#' + more_info_id).slideDown();
            $('#' + book_now_id).slideDown();
        });
    });


    //Book Now Button Clicked
    $('.book-now').on('click', function() {
        let $this = $(this);
        $('.book-now').attr('disabled', true);
        let expert_id = $this.attr('data-expert-id');
        let message_id = 'message' + expert_id;
        $('#' + message_id).text('please wait while fetching Expert data ....');

        $this.find('.loading').show();
        $.ajax({
            url: "/get-expert-appointment-modal",
            type: 'POST',
            data: {
                expert_id: expert_id
            },
            success: function(response) {
                $('body').prepend(response);
                //Apointment Model 
                $('#datepicker').datepicker({
                    startDate: '0d',
                    format: 'yyyy-mm-dd',
                })
                $('#AppointmentModal').modal('show');

            },
            error: function() {

            },
            complete: function() {
                $this.find('.loading').hide();
                $('.book-now').attr('disabled', false);
                $('#' + message_id).text('');

            }
        });



    });

    $(document).on('changeDate', '#datepicker', function() {
        $('#selected_date').val(
            $('#datepicker').datepicker('getFormattedDate')
        );
        $("#selectDateFirst").remove();
        $("#duration .hidden").show();
    });

    $(document).on('change', '#duration', function() {
        let selected_date = $('#selected_date').val();
        let duration = $(this).val();
        let $this = $(this);
        $this.attr('disabled', true);
        $("#time_slot_label .loading").show();
        $('#time_slot #selectDurationFirst').text('plase wait we are fetching time slots...')
        $.ajax({
            url: '/get-expert-available-hours',
            type: 'POST',
            data: {
                expert_id: $('#expert_id').val(),
                duration: duration,
                date: selected_date
            },
            success: function(response) {
                $('#time_slot').empty();
                $('#time_slot')
                    .append($("<option></option>")
                        .attr("value", '')
                        .text(''));
                response.forEach(item => {
                    $('#time_slot')
                        .append($("<option></option>")
                            .attr("value", item.value)
                            .text(item.text));
                });

                $("#time_slot").attr('disabled', false);
                if (response.length == 0) {
                    $('#time_slot')
                        .append($("<option></option>")
                            .attr("value", '')
                            .text('No Times Avaiable For This Date , Please select antoher one'));
                }
            },
            error: function() {

            },
            complete: function() {
                $this.attr('disabled', false);
                $("#time_slot_label .loading").hide();

            }
        })
    });

    $(document).on('submit', '#AppointmentForm', function($event) {
        let form_data = $(this).serialize();
        $("#appointment-info").slideDown();
        $('#appointment-info .loading').show();
        $("#book").attr('disabled', true);
        $("#book").text('Saving..');

        $event.preventDefault();
        $.ajax({
            url: '/make-appointment',
            type: 'POST',
            data: form_data,
            success: function(response) {
                $("#appointment-info .date").text(response.date);
                $("#appointment-info .time-range").text('from ' + response.from_time + ' to ' + response.to_time);
                $("#appointment-info .error").text('');
                let selected_index = document.getElementById('time_slot').selectedIndex;
                console.log(selected_index);
                document.getElementById("time_slot").options[selected_index].remove();
            },
            error: function() {

                $('#appointment-info .error').text('something went wrong please try again')

            },
            complete: function() {
                $('#appointment-info .loading').hide();
                $("#book").attr('disabled', false);
                $("#book").text('Book');


            }
        })
    })

});