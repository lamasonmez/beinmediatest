@extends('layouts.app')

@section('content')
<div class="container-fluid" style="margin-top: 40px;margin-bottom:40px;">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="image-container">
                <img src="{{ asset('/images/login-register.png') }}"  > 
            </div>
        </div>
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <button type="submit" class="btn btn-primary ">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                       
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
