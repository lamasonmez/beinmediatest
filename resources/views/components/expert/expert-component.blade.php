<div class="card expert-card" >
    <div class="card-body">
        <div class="expert-img">
            <img src="{{ asset('/images/user.png') }}">
        </div>
        <div class="expert-info">
            <div class="name">
                {{ $expert->name }}
            </div>
            <div class="expert">
                {{ $expert->expert }}
            </div>
            <div class="more-info-box" id="{{ 'more-info'.$expert->id }}">
                <div class="country">{{ $expert->country }}</div>
                <div class="working-hours">
                    {{ $expert->convertTimeToTimezone($expert->working_hours_from,$expert->time_zone,session()->get('timezone'))->format('h:i A') }} 
                    - 
                    {{ $expert->convertTimeToTimezone($expert->working_hours_to,$expert->time_zone,session()->get('timezone'))->format('h:i A') }}</div>
            </div>
            <button class="more-info-btn more-btn" data-target="{{ $expert->id }}">
                More Info
            </button>
            <button class="more-info-btn book-now" id="{{'book-now'.$expert->id }}" data-expert-id="{{ $expert->id }}">
                <span>Book Now</span>
                    <i class="loading" >
                        <i class="fas fa-spinner fa-spin"></i>
                    </i>
            </button>
            <p class="message" id="{{ 'message'.$expert->id }}"></p>
        </div>
    </div>
</div>