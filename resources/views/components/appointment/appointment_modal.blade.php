<div class="modal fade in" id="AppointmentModal">
    <div class="modal-dialog ">
      <div class="modal-content">
  
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">{{ $expert->name }}</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
  
        <!-- Modal body -->
        <div class="modal-body">
          <form class="appointment-form" id="AppointmentForm">
             @csrf
             <input type="hidden"  id="expert_id" name="expert_id" value="{{ $expert->id }}">
             <input type="hidden"  id="start_datetime" name="start_datetime" value="">
             <input type="hidden"  id="end_datetime" name="end_datetime" value="">
             <input type="hidden" name="client_timezone" value="{{ session()->get('timezone') }}">
              <div class="row">
                  <div class="col-12">
                     <div class="form-group">
                        <label for="date" class="center">Select Date</label>
                        <div id="datepicker"></div>
                        <input required type="hidden" id="selected_date" name="date">
                     </div>
                  </div>
                  <div class="col-12">
                    <div class="form-group">
                       <div id="timezone">Timezone: {{ session()->get('timezone') }}</div>
                    </div>
                 </div>
                 <div class="col-12">
                    <div class="form-group">
                        <label for="duration" class="form-control-label">Duration</label>
                       <select id="duration" class="form-control"  name="duration" required>
                          <option id="selectDateFirst" value="">Plase Select Date First</option>
                          <option class="hidden" value=""></option>
                           <option class="hidden" value="15">15 Min</option>
                           <option class="hidden" value="30">30 Min</option>
                           <option class="hidden" value="45">45 Min</option>
                           <option class="hidden" value="60">1 Hour</option>
                       </select>
                    </div>
                 </div>
                 <div class="col-12">
                    <div class="form-group">
                        <label for="time_slot" class="form-control-label" id="time_slot_label">
                              Time Slot  
                              <i class="loading">
                                 <i class="fas fa-spinner fa-spin"></i>
                              </i>
                        </label>
                       <select id="time_slot" class="form-control" name="time"  required>
                          <option id="selectDurationFirst" value="">Please Select Duration First</option>
                       </select>
                    </div>
                 </div>
                 <div class="col-12">
                  <div class="form-group">
                      <label for="client_name" class="form-control-label">Your Name</label>
                     <input  id="client_name"  type="text" class="form-control" name="client_name" required/>
                     
                  </div>
               </div>
                 <div class="col-12">
                    <div class="form-group">
                       <div id="appointment-info">
                           <div>Your Appointment will be on</div>
                           <div class="date"></div>
                           <div class="time-range"></div>
                           <div class="error"></div>
                           <div class="loading">
                              <i class="fas fa-spinner fa-spin"></i>
                           </div>
                           
                       </div>
                    </div>
                 </div>
                 <div class="col-md-12">
                     <!-- Modal footer -->
                     <div class="modal-footer">
                        <button type="submit" class="btn btn-success submit" id="book">Book</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                     </div>
                 </div>
              </div>
             
          </form>
        </div>
  
       
  
      </div>
    </div>
  </div>