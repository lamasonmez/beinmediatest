@extends('layouts.app')




@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h3>Our Experts</h3>
        </div>
        @foreach($experts as $expert)
            <div class="col-md-4">
                <x-expert.expert-component :expert="$expert"></x-expert.expert-component>
            </div>
        @endforeach
    </div>
</div>
@endsection



